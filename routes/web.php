<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('test', [
    'uses'       => 'ExampleController@test'
]);

$router->post('user/signup', [
    'uses'       => 'UserController@signup'
]);

$router->post('user/login', [
    'uses'       => 'UserController@login'
]);

$router->post('user/property', [
    'uses'       => 'PropertyController@addProperty'
]);

$router->put('user/property', [
    'uses'       => 'PropertyController@updateProperty'
]);

$router->get('user/property', [
    'uses'       => 'PropertyController@getUserProperties'
]);

$router->put('user/property/favourite', [
    'uses'       => 'PropertyController@markFavourite'
]);

$router->get('user/property/favourite', [
    'uses'       => 'PropertyController@getFavourites'
]);

$router->get('property/all', [
    'uses'       => 'PropertyController@getAllProperties'
]);

$router->get('property/detail', [
    'uses'       => 'PropertyController@getPropertyDetail'
]);

$router->post('property/image', [
    'uses'       => 'PropertyController@uploadImage'
]);

$router->get('property/search', [
    'uses'       => 'PropertyController@search'
]);