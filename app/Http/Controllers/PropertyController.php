<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Property;
use App\Models\PropertyImage;
use App\Models\Favourite;
use App\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use App\Resources\PropertyResource;
use App\Resources\FavouriteResource;
use App\Resources\PropertyCollection;
use App\Resources\FavouriteCollection;
use DB;


class PropertyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Add Property
     */
    public function addProperty(Request $request) {

        $this->validate($request, [
            "title" => "required",
            "description" => "required",
            "userId" => "required"
        ]);

        $data = $request->all();

        $user = User::where('id', $data['userId'])->first();

        if (!$user) {
            return $this->sendCustomResponse(false, "No User for this id");
        }

        $property = new Property();
        $data = $request->only($property->getFillable());
        $property->fill($data)->save();
        $user->property()->save($property);

        return $this->sendCustomResponseWithData(true, 'Property Created', new PropertyResource($property));

    }

    /**
     * Upload Property Image
     */
    public function uploadImage(Request $request) {
    
        $propertyId = $request->input('propertyId');
        $imagePath = null;
        $property = Property::where("id", $propertyId)->first();

        if (!$property) {
            return $this->sendCustomResponse(false, "No Property for this id");
        }

        //Delete all images exist for this property
        PropertyImage::where("property_id", "=", $propertyId)->delete();

        if ($request->hasFile("image")) {
            //$imagePath = "storage/" . $request->file("image")->store("property_image");
            $photoName = time(). '.' . $request->file("image")->getClientOriginalExtension();
            $imagePath = $request->file("image")->move('property_images', $photoName);
        } else {
            return $this->sendCustomResponse(false, "Error uploading image");
        }

        $propertyImage = new PropertyImage();
        $propertyImage->property_id = $propertyId;
        $propertyImage->image_path = $imagePath;
        $property->propertyImage()->save($propertyImage);

        return $this->sendCustomResponseWithData(true, 'Image Uploaded', new PropertyResource($property)); 

    }

    /**
     * Get User Added Properties
     */
    public function getUserProperties(Request $request) {

        $this->validate($request, [
            "userId" => "required"
        ]);
        $userId = $request->input('userId');
        $properties = Property::select("property.*", "favourite.id as favourite")->leftJoin('favourite', function ($join) use($userId) {
                $join->on('property.id', '=', 'favourite.property_id')
                            ->where("favourite.user_id", "=", $userId)
                            ->where("favourite.status", "=", 1);

            })->where("property.user_id", "=", $userId)->orderBy('id')->get();

        return $this->sendCustomResponseWithData(true, '', new PropertyCollection($properties));

    }

    /**
     * Get All Properties in Database
     */
    public function getAllProperties(Request $request) {

        $userId = NULL;

        if ($request->input('userId')) {
            $userId = $request->input('userId');
        } else {
            $userId = NULL;
        }
            
        $properties = Property::select("property.*", "favourite.id as favourite")->leftJoin('favourite', function ($join) use($userId) {
            $join->on('property.id', '=', 'favourite.property_id')
                        ->where("favourite.user_id", "=", $userId)
                        ->where("favourite.status", "=", 1);
        })->orderBy('id')->get();
        
        return $this->sendCustomResponseWithData(true, '', new PropertyCollection($properties));
    }


    /**
     * Update Property
     */
    public function updateProperty(Request $request) {

        $this->validate($request, [
            "userId" => "required",
            "id" => "required",
        ]);

        $requestData = $request->all();
        $property = Property::where("id", $requestData['id'])->first();

        if (!$property) {
            return $this->sendCustomResponse(false, "No Property for this id");
        }

        $data = $request->only($property->getFillable());
        $property->fill($data)->save();

        return $this->sendCustomResponseWithData(true, 'Property Updated', new PropertyResource($property));

    }

    /**
     * Mark Property as favourite
     */
    public function markFavourite(Request $request) {

        $this->validate($request, [
            "userId" => "required",
            "propertyId" => "required",
            "status" => "required|boolean",
        ]);

        $data = $request->all();
        $matchThese = array("user_id" => $data["userId"], "property_id" => $data["propertyId"]);
        $favourite = Favourite::updateOrCreate($matchThese, ["status" => $data["status"]]);
        return $this->sendCustomResponseWithData(true, '', new FavouriteResource($favourite));
    }

    /**
     * Get all favourite property of user
     */
    public function getFavourites(Request $request) {

        $this->validate($request, [
            "userId" => "required"
        ]);

        $userId = $request->input('userId');
        $favourites = Favourite::where("user_id", $userId)->where("status", 1)->get();
        return $this->sendCustomResponseWithData(true, '', new FavouriteCollection($favourites));
    }

    public function search(Request $request) {

        $userId = NULL;
        $query = $request->input('query');

        if ($request->input('userId')) {
            $userId = $request->input('userId');
        } else {
            $userId = NULL;
        }

        $properties = Property::select("property.*", "favourite.id as favourite")
            ->leftJoin('favourite', function ($join) use($userId) {
                $join->on('property.id', '=', 'favourite.property_id')
                        ->where("favourite.user_id", "=", $userId)
                        ->where("favourite.status", "=", 1);
        })->where("title", "LIKE", "%" . $query . "%")
          ->orWhere("address", "LIKE", "%" . $query . "%")
          ->orderBy('id')->get();

        return $this->sendCustomResponseWithData(true, '', new PropertyCollection($properties));

    }


    /**
     * Unused
     */
    public function getPropertyDetail(Request $request) {

        $userId = $request->input('userId');
        $propertyId = $request->input('propertyId');
        
    }


}
