<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use App\Resources\LoginResource;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }


    public function signup(Request $request) {

        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'gender'=> 'required|in:MALE,FEMALE' 
        ]);

        $data = $request->all();

        $user = User::where('email', $data['email'])->first();

        if ($user) {
            return $this->sendCustomResponseWithData(false, "User already exist with this email", null);
        } else {
            
            $user = new User();
            $data = $request->only($user->getFillable());
            $user->fill($data)->save();
            $user->password = Hash::make($data['password']);
            $user->save();

            return $this->sendCustomResponseWithData(true, "Signup Successfully", new UserResource($user));
        }

    }

    public function login(Request $request) {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];

        $user = User::where('email', $email)->first();

        if (!$user) {
            return $this->sendCustomResponseWithData(false, "Login Fail", null);
        } 

        if (!Hash::check($password, $user->password)) {
            return $this->sendCustomResponseWithData(false, "Login Fail", null);
        }

        return $this->sendCustomResponseWithData(true, "Login Successfully", $user);

    }
}
