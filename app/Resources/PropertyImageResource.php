<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PropertyImageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'property_id' => $this->property_id,
            'image_path' => $this->image_path,
        ];
    }
}