<?php
namespace App\Resources;

use App\Models\Property;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PropertyCollection extends ResourceCollection {

	public function toArray($request) {

		$this->collection->transform(function (Property $property) {
			return (new PropertyResource($property));
		});

		return parent::toArray($request);
	}

}
