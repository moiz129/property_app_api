<?php
namespace App\Resources;

use App\Models\Favourite;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FavouriteCollection extends ResourceCollection {

	public function toArray($request) {

		$this->collection->transform(function (Favourite $favourite) {
			return (new FavouriteResource($favourite));
		});

		return parent::toArray($request);
	}

}
