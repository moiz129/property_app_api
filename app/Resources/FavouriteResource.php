<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Resources\PropertyResource;

class FavouriteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => (bool) $this->status,
            'property' => new PropertyResource($this->property)
        ];
    }
}