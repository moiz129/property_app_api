<?php
namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Resources\PropertyImageResource;

class PropertyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                 => $this->id,
            'title'              => $this->title,
            'description'        => $this->description,
            'address'            => $this->address,
            'type'               => $this->type,
            'category'           => $this->category,
            'country'            => $this->country,
            'city'               => $this->city,
            'phone_of_seller'    => $this->phone_of_seller,
            'area_unit'          => $this->area_unit,
            'area'               => $this->area,
            'price'              => $this->price,
            'bedrooms'           => $this->bedrooms,
            'bathrooms'          => $this->bathrooms,
            'kitchen'            => $this->kitchen,
            'year_build'         => $this->year_build,
            'favourite'          => !$this->favourite && empty($this->favourite) ? false : true,
            'images'             => PropertyImageResource::collection($this->propertyImage)
        ];
    }
}
