<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Property;

class PropertyImage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property_image';
    public $timestamps = false;

    protected $fillable = [
        'property_id',
        'image_path',
    ];

    public function property() {
        return $this->belongsTo(Property::class);
    }

}
