<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Favourite;
use App\Models\PropertyImage;

class Property extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'title',
        'description',
        'address',
        'type',
        'category',
        'country',
        'city',
        'phone_of_seller',
        'area_unit',
        'area',
        'price',
        'bedrooms',
        'bathrooms',
        'kitchen',
        'year_build'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function favourite() {
        return $this->hasMany(Favourite::class);
    }

    public function propertyImage() {
        return $this->hasMany(PropertyImage::class);
    }

}

